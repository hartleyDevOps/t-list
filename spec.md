# t-list Specification

t-list displays a virtual, 'infinite' list. The template inside the t-list element represents the DOM to create for each list item. The items property specifies an array of list item data.

### Component Use

``` html

<t-list
  resources="{{resources}}"
  settings="{{settings}}"
  list="{{results}}"
  filter="{{filters}}"
  sorting="{{sorting}}"
>
</t-list>

```


### Options to Component

```javascript

	var resources = {
	    "icons": {
	      "search": "search-icon",
	      "filter": "filter-icon",
	      "list": "list-icon",
	      "grid": "grid-icon",
	      "sort": "sort-icon",
	      "sortByAsc": "arrow-down-icon",
	      "sortByDesc": "arrow-up-icon",
	      "listView": "list-view-icon",
	      "gridView": "grid-view-icon",
	      "close": "close-icon"
	    },
	    "messages": {
	      "resultsFound": "We found [[resultCount]] Activities for your search.",
	      "noResultsFound": "Couldn't find any Activities with your search",
	      "noResultsFiltered": "Couldn't find any Activities with your filters",
	      "noResultsfilteredForSearchTerm": "Couldn't find any Activity names containing [[searchTerm]]."
	    },
	    "labels": {
	    	"grid": "Grid",
	    	"list": "List"
	    },
	    "events": {
	    	"removeFilter": "_removeFilter"
	    }
	  }

```

```javascript

	  var settings = {
	    "showPerPage": 20,
	    "pageNumber": 1,
	    "scrollToPage": 2
	  }

```


#### Results Data

```javascript

	var results = [
		    {
		      "title": "This is an activity name",
		      "description": "Beans are cool and they are beans. Eat them. They are yummy",
		      "category": {
		        "name": "Family Friendly"
		      },
		      "thumbnail": "http://www.dynamic.viator.com/graphicslib/6801/SITours/cool-beans-mod-in-las-vegas-168521.jpg",
		      "isCancellable": true,
		      "allPassengersInfoRequired": false,
		      "isGuaranteeRequired": true,
		      "name": "Cool Beans MOD",
		      "id": "",
		      "fare": {
		        "amount": 10.37,
		        "currency": "USD"
		      }
		    },
		    {
		      "title": "This is an activity name",
		      "description": "Beans are cool and they are beans. Eat them. They are yummy",
		      "category": {
		        "name": "Family Friendly"
		      },
		      "thumbnail": "http://www.dynamic.viator.com/graphicslib/6801/SITours/cool-beans-mod-in-las-vegas-168521.jpg",
		      "isCancellable": true,
		      "allPassengersInfoRequired": false,
		      "isGuaranteeRequired": true,
		      "name": "Cool Beans MOD",
		      "id": "",
		      "fare": {
		        "amount": 12.55,
		        "currency": "USD"
		      }
		    }
		  ]
```

#### Filter Data

```javascript

	var filters = {
		  "labels": {
		    "heading": "Filter Activity/Hotel By"
		  },
		  "groups": [
		    {
		      "label": "Hotel/Activity Name",
		      "type": "Autosuggest",
		      "placeholder": "Search by activity/hotel name",
		      "isOpen": true,
		      "showMoreAfter": 5,
		      "onChange": "_updateFilterQuery",
		      "selectedValue": null,
		      "list": [
		        {
		          "label": "Las Vegas Hilton Delux"
		        },
		        {
		          "label": "Las Vegas Hilton Delux"
		        },
		        {
		          "label": "Las Vegas Hilton Delux"
		        }
		      ]
		    },
		    {
		      "label": "Price/Distance",
		      "type": "Range",
		      "isOpen": false,
		      "showMoreAfter": 5,
		      "onChange": "_updateFilterQuery",
		      "selectedValue": "35.10",
		      "minPrice": {
		        "currency": "USD",
		        "value": "9.10"
		      },
		      "maxPrice": {
		        "currency": "USD",
		        "value": "158.81"
		      }
		    },
		    {
		      "label": "Rating",
		      "type": "Options",
		      "isOpen": false,
		      "showMoreAfter": 5,
		      "onChange": "_updateFilterQuery",
		      "options": [
		        {
		          "rating": 5,
		          "selected": false,
		          "count": 12
		        },
		        {
		          "rating": 4,
		          "selected": false,
		          "count": 22
		        },
		        {
		          "rating": 3,
		          "selected": false,
		          "count": 34
		        },
		        {
		          "rating": 2,
		          "selected": false,
		          "count": 8
		        },
		        {
		          "rating": 1,
		          "selected": false,
		          "count": 5
		        }
		      ]
		    },
		    {
		      "label": "Category/Brand/Amenities",
		      "type": "Options",
		      "isOpen": false,
		      "showMoreAfter": 5,
		      "onChange": "_updateFilterQuery",
		      "autosuggest": {
		        "enabled": true,
		        "placeholder": "Search by category name"
		      },
		      "options": [
		        {
		          "label": "Family Friendly",
		          "selected": false
		        },
		        {
		          "label": "Transfers &amp; Ground Transport",
		          "selected": false
		        }
		      ]
		    }
		  ]
		}
```

#### Sorting Options

```javascript

	var sorting = [
		  {
		    "selected": "price",
		    "sortOptions": [
		      {
		        "type": "price",
		        "label": "Price",
		        "defaultSortBy": "desc",
		        "onClick": "_sortBy('price')"
		      },
		      {
		        "type": "name",
		        "label": "Name",
		        "defaultSortBy": "asc",
		        "onClick": "_sortBy('name')"
		      },
		      {
		        "type": "category",
		        "label": "Category",
		        "defaultSortBy": "desc",
		        "onClick": "_sortBy('category')"
		      }
		    ]
		  }
		]

```


## Important Information

t-list will be using two components inside.
- t-filter
- t-activity-item

###### t-filter
If parent view explicitly asks to show price or count with option filter by setting, component handles this to show respective value along with filter


###### t-activity-item
t-activity-item element represents the DOM to create for each activity item. The `items` property specifies an array of activity-list item data.


## Important Information

- Misc items to be handled from CSS like:
1. List card background color
2. Mixin (to control text color, font etc)
3. Mixin (Button color, size etc.)


## Test Cases
- Basic validation for filter

## Steps to Start
- Set Github repository at your end for this project, we will merge them later
- APIs Integration - Use Tavisca's APIs for integration purpose.

## Performance standard
- Any component if opened via [web page tester](https://www.webpagetest.org/), it should load under 500ms (milli seconds).

## Documents
- Visual designs for search components - https://projects.invisionapp.com/share/6E9PJ7R4Q#/screens/212067485
- API access : Url - http://demo.travelnxt.com/dev
- Tavisca Elements - https://github.com/atomelements and https://github.com/travelnxtelements
- Vaadin elements - https://vaadin.com/docs/-/part/elements/elements-getting-started.html
- Google - https://elements.polymer-project.org/browse?package=google-web-components
- Tavisca Web component style Guide - https://drive.google.com/open?id=0B7BT_2nBFNYVR2tscE9pRnVJYmc

## Ballpark Estimates

60 Days / 12 Weeks
- Estimates are including above requrement with responsive design.
- Estimates are for single resourse.
- These are ballpark estimates and can be more/less while actual development.
